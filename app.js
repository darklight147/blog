const express = require('express');
const bodyParser = require('body-parser');
const ejs  = require('ejs');
const lodash = require('lodash');

const app = express();
const PORT = process.env.PORT || 3000;
let posts = [];
//content
const homestarting = "Mohamed is the best person in the world ";
const aboutcontent = "About content";
const contactcontent = "Contact content";
//paths
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }))
//view engine
app.set('view engine', 'ejs');

//get requests
app.get('/', (req, res) => {
    res.render('home', {fp: homestarting, A: posts});
    
});
app.get('/about', (req, res) => {
    res.render('about', {p : aboutcontent});
})
app.get('/contact', (req, res) => {
    res.render('contact', {p: contactcontent});
});
app.get('/compose', (req, res) => {
    res.render('compose');
});
app.get('/posts/:postTitle', (req, res) => {

    posts.forEach(element => {
        var count = 0;
        if (lodash.lowerCase(element.title) === lodash.lowerCase(req.params.postTitle) ) {
            count = 1;
            res.render('post', {title :element.title, content : element.post});
        }

    });
    
});

//post requests
app.post('/compose', (req, res) => {

    let POST = {title : req.body.title, post: req.body.post};
    posts.push(POST);
    
    res.redirect('/');
    
    
});


//code HERE




//start server
app.listen(PORT, () => {
    console.log('running on port :' + PORT);
});
